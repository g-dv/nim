# Nim

This program allows you to play Nim against an AI which uses the Nim sum (game theory).

## Rules

The players take turn choosing a line and substracting a number of items from that line. The player who removes the last item wins.

## Configuration

The configuration of the initial board can easily be changed. The default one gives the human player a chance of winning the game.

## Theory (in French)

The AI plays by trying to set the grundy value to 0.

These definitions and theorems are taken from David Madore's lecture on game theory. The complete course is available online on his website.

### Somme de nim

**Définition.** Soient $`G_1,G_2`$ deux jeux combinatoires impartiaux dont on note $`x_1,x_2`$ les positions initiales et $`E_1,E_2`$ les relations d'arêtes. On appelle **somme de nim** (ou simplement « somme ») de $`G_1`$ et $`G_2`$, et on note $`G_1 \oplus G_2`$ le jeu combinatoire impartial dont
 - l'ensemble des positions est $`G_1 \times G_2`$,
 - la relation d'arête (définissant le graphe) est $`(E_1 \times id_{G_2}) \cup (id_{G_1} \times E_2)`$, c'est-à-dire que les voisins sortants de $`(y_1,y_2) \in G_1 \times G_2`$ sont les $`(z_1,y_2)`$ avec $`z_1`$ voisin sortant de $`y_1`$ ainsi que les $`(y_1,z_2)`$ avec $`z_2`$ voisin sortant de $`y_1`$, et
 - la position initiale est $`(x_1,x_2)`$.

### Somme de nim d'ordinaux

**Définition.** Soient $`\alpha_1,\alpha_2`$ deux ordinaux. On appelle **somme de nim** de $`\alpha_1`$ et $`\alpha_2`$ et on note $`\alpha_1 \oplus \alpha_2`$ l'ordinal défini inductivement par

```math
\alpha_1 \oplus \alpha_2 = mex\big( \{\beta_1\oplus\alpha_2 : \beta_1 < \alpha_1\} \cup\{\alpha_1\oplus\beta_2 : \beta_2 < \alpha_2\}\big)
```

Autrement dit, il s'agit du plus petit ordinal qui n'est ni de la forme $`\beta_1\oplus\alpha_2`$ pour $`\beta_1 < \alpha_1`$ ni de la forme $`\alpha_1\oplus\beta_2`$ pour $`\beta_2 < \alpha_2`$.  Encore autrement, il s'agit de la valeur de Grundy du jeu $`(*\alpha_1) \oplus (*\alpha_2)`$.

### Somme de nim : entre jeux et ordinaux

**Théorème.** Si $`G_1,G_2`$ sont deux jeux combinatoires impartiaux bien-fondés ayant valeurs de Grundy respectivement $`\alpha_1,\alpha_2`$, alors la valeur de Grundy de $`G_1\oplus G_2`$ est $`\alpha_2\oplus\alpha_2`$.

### Application

La somme de nim d'ordinaux peut se calculer en écrivant les ordinaux en question en binaire et en effectuant le *ou exclusif* de ces écritures binaires (c'est-à-dire que le coefficient devant chaque puissance de $`2`$ donnée vaut $`1`$ lorsque exactement l'un des coefficients des nombres ajoutés vaut $`1`$, et $`0`$ sinon).

La valeur de Grundy de la somme de nim de jeux combinatoires impartiaux bien-fondés se calcule donc comme le *ou exclusif* des valeurs de Grundy des composantes.  Notamment, la valeur de Grundy d'un jeu de nim est le *ou exclusif* des nombres d'allumettes des différentes lignes.

Enfin, la valeur de Grundy d'un jeu combinatoire impartial bien-fondé $`G`$ peut se voir comme l'unique ordinal $`\alpha`$ tel que le second joueur ait une stratégie gagnante dans $`G \oplus *\alpha`$.
