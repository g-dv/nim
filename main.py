"""Nim Game

This program allows you to play Nim against an AI.

The players take turn choosing a line and substracting a number of matches
from that line. The player who removes the last match wins.

The AI plays by trying to set the grundy value to 0.
The explanation of this method is in the README.
"""


# This value can be changed
INITIAL_BOARD = [1, 2, 3, 4, 5]


def getGrundyValue(board):
    res = 0
    for item in board:
        res ^= item  # the nim sum is equivalent to XOR
    return res


def getUpdatedBoard(line, nToErase):
    res = board[:]  # actually copy the list
    res[line] -= nToErase
    return res


def playAnyValidMove():
    for line, maxAmount in enumerate(board):
        for i in range(maxAmount):
            return getUpdatedBoard(line, i + 1)


def playAsAI():
    for line, maxAmount in enumerate(board):
        for i in range(maxAmount):
            if getGrundyValue(getUpdatedBoard(line, i + 1)) == 0:
                # we found a winning move
                return getUpdatedBoard(line, i + 1)
    return playAnyValidMove()  # we didn't find a winning move


def displayBoard():
    print("")
    for i in range(len(board)):
        print("\t", i, end=": ")
        for _ in range(board[i]):
            print("|", end=" ")
        print("")
    print("")


def isGameOver():
    return board == [0] * len(board)


def playAsPlayer():
    line = amount = -1
    while line < 0 or line >= len(board) or not board[line]:
        print("\tline number: ", end="")
        line = int(input())
    while amount < 0 or amount > board[line]:
        print("\tamount to erase: ", end="")
        amount = int(input())
    return getUpdatedBoard(line, amount)


board = INITIAL_BOARD
noPlayer = 2
while not isGameOver():
    noPlayer = noPlayer % 2 + 1  # toggle current player
    displayBoard()
    print("Turn of player", noPlayer)
    if noPlayer == 1:
        board = playAsPlayer()
    else:
        board = playAsAI()
displayBoard()
print("Player", noPlayer, "won")
